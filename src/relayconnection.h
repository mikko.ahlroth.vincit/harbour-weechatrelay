/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#ifndef RELAYCONNECTION_H
#define RELAYCONNECTION_H

#include <QObject>
#include <QTcpSocket>
#include <QByteArray>

class RelayConnection : public QObject
{
    Q_OBJECT

public:
    explicit RelayConnection(QTcpSocket* socket, QObject* parent = 0);

    QByteArray read(quint64 bytes);
    qint64 bytesAvailable() const;

public slots:
    void connect(QString host, uint port);
    void disconnect();
    void write(QByteArray data);

    void socketConnected();
    void socketDisconnected();
    void socketReadyRead();

signals:
    void connected();
    void disconnected();
    void newDataAvailable();

protected:
    QTcpSocket* socket;
};

#endif // RELAYCONNECTION_H
