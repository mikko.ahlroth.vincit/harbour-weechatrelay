/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#include "protocolhandler.h"

ProtocolHandler::ProtocolHandler(QObject* parent):
    QObject(parent),
    bytesNeeded(0)
{
}

void ProtocolHandler::emitData(QString data)
{
    QByteArray bytes;
    bytes.append(data);
    emit sendData(bytes);
}

QDataStream* ProtocolHandler::read(RelayConnection* connection, quint64 bytes)
{
    QByteArray data = connection->read(bytes);
    return new QDataStream(data);
}
