/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

/*
 * This module handles the internal buffer list and buffer related events of the client.
 */

.pragma library

.import QtQuick 2.0 as QQ

.import "eventqueue.js" as EQ
.import "moment.js" as M
.import "debug.js" as D
.import "weechatcolors.js" as WC

// Change line into an object insertable into a QML ListModel
function lineToModelItem(moment, prefix, str) {
    return {
        time: moment.format('HH:mm:ss'),
        prefix: prefix,
        str: str
    };
}

// Create a generic QML ListModel
function createListModel(parent) {
    if (typeof parent === 'undefined') {
        parent = _parentPage;
    }

    return Qt.createQmlObject('import QtQuick 2.0; ListModel {}',
                              parent);
}

// Dict of buffers, pointer as key, for fast referencing
var _buffers = {};

// List of all buffers for easy iteration, will be sorted ascending
// according to buffer number whenever requested
var _buffersList = [];

// ListModel for BufferList.qml, will be kept in sync with _buffersList
var _buffersListModel = null;

// Page that will own all of our textlistmodels
var _parentPage = null;



// A class for storing the information of a single buffer
function Buffer(pointer, number, full_name, name, title) {
    this.pointer = pointer;
    this.number = number;
    this.full_name = full_name;
    this.name = name;
    this.title = title;
    this.visible = false;

    // Amount of unread lines in this buffer
    this.unreadCount = 0;

    // Has this buffer had its initial sync done?
    // (Has it ever been active?)
    this.inited = false;

    // A listmodel where we put the messages of this buffer
    this.textList = createListModel();

    // Add line to this buffer
    this.add = function(moment, prefix, str) {
        this.textList.insert(0, lineToModelItem(moment, prefix, str));

        if (!this.active) {
            ++this.unreadCount;

            _updateBufferInList(this);
        }
    };

    // Replace the line contents of this buffer with the given history
    this.addHistory = function(history) {
        this.textList.clear();

        lineAdded(history);
    }

    // This buffer has become visible to the user
    this.activate = function() {
        if (!this.inited && this.pointer !== '0') {
            var self = this;
            var lines = this.textList.count;
            EQ.command('hdata buffer:0x' + this.pointer + '/own_lines/last_line(-' + (lines + 100) + ')/data', function(result) {
                var lines = result[0].objectSets;
                lines.reverse();
                handleLines(lines);
            });
        }

        this.active = true;
        this.inited = true;
        this.unreadCount = 0;
        _updateBufferInList(this);
    }

    // This buffer will be hidden from the user
    this.deactivate = function() {
        this.active = false;
    }

    // Send input into this buffer
    this.input = function(str) {
        EQ.command('input ' + this.full_name + ' ' + str);
    };
}




function init() {
    EQ.addHandler("__weecrapp_ready", connected);
    EQ.addHandler("_buffer_line_added", lineAdded);
}

// Set the given page to be used for the core buffer
// This is so that we can reuse the debug page as the
// WeeChat core buffer
function setDebugPage(page) {
    _parentPage = page;

    var pointer = '0';
    var buffer = new Buffer(pointer, 1, 'core.weechat', 'weechat', 'Debug/core');
    _buffers[pointer] = buffer;
    _buffersList.push(buffer);

    _buffersListModel.append(_bufferToModel(buffer));

    D.setDebugBuffer(buffer);
    page.changeBuffer(buffer);
}

// Get a single buffer by pointer
function getBuffer(pointer) {
    if (_buffers.hasOwnProperty(pointer)) {
        return _buffers[pointer];
    }

    return null;
}

// Return bufferlist sorted by buffer number
function getBuffersByNumber() {
    return _buffersList;
}

// If bufferlistmodel was not created yet, create it and return
function getBufferListModel(parent) {
    if (_buffersListModel === null) {
        _buffersListModel = createListModel(parent);
    }

    return _buffersListModel;
}


function connected() {
    D.d("Fetching all buffers...");
    EQ.command("hdata buffer:gui_buffers(*) full_name,short_name,number,type,title", bufferListData);
    EQ.command("sync");
}

function disconnected() {

}


// Handle incoming buffer list data
function bufferListData(data) {
    data = data[0];
    var buffers = data.objectSets;

    for (var i = 0; i < buffers.length; ++i) {
        var pointer = buffers[i]['__path'][0];
        var number = buffers[i].number;
        var name = buffers[i].short_name;
        var full_name = buffers[i].full_name;
        var title = buffers[i].title;
        var buffer =  null;

        if (buffers[i].type !== 0) {
            // Currently only sync IRC buffers
            continue;
        }

        // If this buffer is the core, merge it to the debug buffer
        if (_parentPage !== null && number === 1 && buffers[i].full_name === 'core.weechat') {
            _buffers[pointer] = _buffers['0'];
            buffer = _buffers['0'];
            buffer.pointer = pointer;
            buffer.full_name = full_name;
            buffer.name = name;
            buffer.title = title;
            buffer.number = number;
        }
        else {
            buffer = new Buffer(pointer, number, full_name, name, title);
            _buffers[pointer] = buffer;
            _buffersList.push(buffer);
        }
    }

    _sortBuffersList();
    _regenerateBufferList();
}

// Handle a single line
function handleLine(line) {
    var date = M.moment(line.date);
    var prefix = WC.codedText2StyledText(WC.parseString(line.prefix));
    var message = WC.codedText2StyledText(WC.parseString(line.message));
    _buffers[line.buffer].add(date, prefix, message);
}

// Handle a list of lines
function handleLines(lines) {
    for (var i = 0; i < lines.length; ++i) {
        handleLine(lines[i]);
    }
}

// New lines were added to a buffer
function lineAdded(data) {
    handleLines(data[0].objectSets);
}





// Keep the buffer list sorted
function _sortBuffersList() {
    _buffersList.sort(function (a, b) {
        return a.number - b.number;
    });
}

// Convert a buffer object into an object insertable into a ListModel
function _bufferToModel(buffer) {
    return {
        pointer: buffer.pointer,
        number: buffer.number,
        name: buffer.name,
        unreadCount: buffer.unreadCount
    };
}

// Update the given buffer in the list
// If the given buffer is not in the list, nothing is done
function _updateBufferInList(buffer) {
    var modelObject = _bufferToModel(buffer);
    var bufferCount = _buffersListModel.count;

    for (var i = 0; i < bufferCount; ++i) {
        var bufferAt = _buffersListModel.get(i);

        if (bufferAt.number === modelObject.number) {
            _buffersListModel.set(i, modelObject);
        }
    }
}

// Regenerate _buffersListModel based on _buffersList
function _regenerateBufferList() {
    _buffersListModel.clear();

    for (var i = 0; i < _buffersList.length; ++i) {
        var modelObject = _bufferToModel(_buffersList[i]);
        _buffersListModel.append(modelObject);
    }
}
