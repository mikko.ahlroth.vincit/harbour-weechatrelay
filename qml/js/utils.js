/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

// Miscellaneous functions for generic use

.pragma library

// Escape strings for Text.StyledText
function escapeStyled(string) {
    string = string.replace('&', '&amp;');
    string = string.replace('<', '&lt;');
    return string.replace('>', '&gt;');
}

// Return StyledText in a specified color
function colored(color, string) {
    return '<font color="' + color + '">' + string + '</font>';
}

// Return bold text
function bold(string) {
    return '<b>' + string + '</b>';
}
