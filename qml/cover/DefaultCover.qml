/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id: defaultCover

    Label {
        id: label
        anchors.centerIn: parent
        text: "WeeCRApp"
    }
}


